package utils;

public class ComplexNumber {
    private double real;
    private double imag;

    public ComplexNumber(double real, double imag) {
        this.real = real;
        this.imag = imag;
    }

    public double abs() {
        return Math.sqrt(real * real + imag * imag);
    }

    public ComplexNumber multiply(ComplexNumber other) {
        double realPart = this.real * other.real - this.imag * other.imag;
        double imagPart = this.real * other.imag + this.imag * other.real;
        return new ComplexNumber(realPart, imagPart);
    }

    public ComplexNumber add(ComplexNumber other) {
        double realPart = this.real + other.real;
        double imagPart = this.imag + other.imag;
        return new ComplexNumber(realPart, imagPart);
    }
}


