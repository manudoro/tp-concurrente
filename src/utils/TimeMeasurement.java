package utils;

public class TimeMeasurement {
    private long start;
    private long finish;

    public TimeMeasurement(){
        this.start= 0;
        this.finish = 0;

    }

    public void start() {
        this.start = System.currentTimeMillis();
    }

    public void stop() {
        this.finish = System.currentTimeMillis();
    }

    public double delayedTime() {
        return (double) (this.finish - this.start) / 1000;
    }
}
