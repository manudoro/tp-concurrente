package task;

import utils.ComplexNumber;

import java.awt.*;
import java.awt.image.BufferedImage;

public class MandelbrotTask extends Task {
    private int alto;
    private int ancho;
    private double x_inicial;
    private double y_inicial;
    private double x_rango;
    private double y_rango;
    private int cant_iteraciones;
    private int fila;
    private BufferedImage imagenFinal;

    public MandelbrotTask(int alto, int ancho, double x_inicial, double y_inicial, double x_rango, double y_rango, int cant_iteraciones, int fila, BufferedImage imagenFinal, Buffer buffer) {
        this.alto = alto;
        this.ancho = ancho;
        this.x_inicial = x_inicial;
        this.y_inicial = y_inicial;
        this.x_rango = x_rango;
        this.y_rango = y_rango;
        this.cant_iteraciones = cant_iteraciones;
        this.fila = fila;
        this.imagenFinal = imagenFinal;
    }

    @Override
    public void run() {
        int[] r = new int[cant_iteraciones + 1];
        int[] g = new int[cant_iteraciones + 1];
        int[] b = new int[cant_iteraciones + 1];
        r[cant_iteraciones] = 0;
        g[cant_iteraciones] = 0;
        b[cant_iteraciones] = 0;

        Color[] colors = new Color[cant_iteraciones];
        for (int i = 0; i < cant_iteraciones; i++) {
            int rgb = Color.HSBtoRGB(i / 256f, 1, 1);
            r[i] = (rgb >> 16) & 255;
            g[i] = (rgb >> 8) & 255;
            b[i] = rgb & 255;
            colors[i] = new Color(r[i], g[i], b[i]);
        }

        for (int columna = 0; columna < ancho; columna++) {
            double x = x_inicial + (columna / (double) ancho) * x_rango;
            double y = y_inicial + (fila / (double) alto) * y_rango;
            ComplexNumber c = new ComplexNumber(x, y);
            ComplexNumber z = new ComplexNumber(0, 0);
            int n = 0;

            while (z.abs() <= 2 && n < cant_iteraciones) {
                z = z.multiply(z).add(c);
                n++;
            }

            int rgb;
            if (n == cant_iteraciones) {
                rgb = 0;
            } else {
                rgb = colors[n].getRGB();
            }

            synchronized (imagenFinal) {
                imagenFinal.setRGB(columna, fila, rgb);
            }
        }
    }

}

