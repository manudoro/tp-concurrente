package task;

/* Una clase PoisonPill que hereda de Task y que hace que el Worker que la tome
termine su ejecucion (puede ser por medio de una excepcion, como en la practica).*/

import exception.PosionPillException;

public class PoisonPill extends Task {
    @Override
    public void run() {
        throw new PosionPillException();
    }
}
