package task;

public abstract class Task implements Runnable{

    @Override
    public abstract void run();
}
