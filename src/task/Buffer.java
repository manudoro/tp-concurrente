package task;

import task.Task;

/**
 Una clase thread.task.Buffer (implementada como un monitor utilizando métodos synchronized)
 que actúa como una cola FIFO concurrente de capacidad acotada. Es decir,
 bloquea a un lector intentando sacar un elemento cuando está vacía y bloquea a
 un productor intentando agregar un elemento cuando está llena. La capacidad del
 thread.task.Buffer también debe ser un parámetro configurable desde la clase Main.
 */

public class Buffer {
    private Task[] data;
    private int N;
    private int begin=0;
    private int end  =0;

    public Buffer(int tamano) {
        this.N = tamano;
        this.data = new Task[this.N + 1];
    }

    public synchronized void push ( Task o ) {
        while (isFull())
            try {
                wait();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        data [ begin ] = o ;
        begin = next ( begin );
        notifyAll ();
    }

    public synchronized Task pop () {
        while ( isEmpty())
            try {
                wait();
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        Task result = data[end];
        end = next( end );
        notifyAll();
        return result;
    }

    private boolean isEmpty() { return begin == end; }
    private boolean isFull() { return next( begin ) == end; }
    private int next(int i) { return (i+1)%(this.N+1); }


}