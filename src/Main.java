import task.Buffer;
import threads.ThreadPool;
import task.MandelbrotTask;
import task.PoisonPill;
import threads.WorkerCounter;
import utils.TimeMeasurement;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        int alto = 600;
        int ancho = 800;
        double x_inicial = -2.0;
        double y_inicial = -1.0;
        double x_rango = 3.0;
        double y_rango = 2.0;
        int cant_iteraciones = 100;
        int numThreads = 16; // Número de hilos
        int bufferCapacity = 10; // Capacidad del buffer
        TimeMeasurement timer = new TimeMeasurement();
        timer.start();

        BufferedImage imagenFinal = new BufferedImage(ancho, alto, BufferedImage.TYPE_INT_RGB);
        Buffer buffer = new Buffer(bufferCapacity);
        ThreadPool threadPool = new ThreadPool(numThreads, buffer, new WorkerCounter());
        threadPool.startThreads();

        for (int fila = 0; fila < alto; fila++) {
            MandelbrotTask task = new MandelbrotTask(alto, ancho, x_inicial, y_inicial, x_rango, y_rango, cant_iteraciones, fila, imagenFinal, buffer);
            buffer.push(task);
        }

        for(int i = 0; i< numThreads; i++) {
            buffer.push(new PoisonPill());
        }
        threadPool.getWorkerCounter().finish();

        timer.stop();
        System.out.println(timer.delayedTime());
        // Guardar la imagen en un archivo
        try {
            ImageIO.write(imagenFinal, "png", new File("Mandelbrot.png"));
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}

