package threads;
/**
 Una clase threads.WorkerCounter (implementada como un monitor utilizando métodos
 synchronized) que evita que Main termine su ejecución mientras queden threads
 trabajando
 */

public class WorkerCounter {

    private int counter = 0;

    public synchronized void increment(){counter++;}

    public synchronized void decrement() throws InterruptedException{
        counter--;
        if(counter==0){
            notify();
        }

    }

    public synchronized void finish() throws InterruptedException{
        while(counter>0){

            this.wait();
        }
    }

}
