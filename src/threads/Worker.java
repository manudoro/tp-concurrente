package threads;

import task.Buffer;
import exception.PosionPillException;

/**
 * Una clase thread.worker.Worker que extienda de Thread y que tome tareas (clase task.Task) de un
 * buffer hasta tomar una señal de terminación (Poison Pill)
 */
public class Worker extends Thread {
    private final Buffer buffer;
    private final WorkerCounter workerCounter;

    public Worker(Buffer buffer, WorkerCounter workerCounter) {
        this.buffer = buffer;
        this.workerCounter = workerCounter;
        workerCounter.increment();
    }

    @Override
    public void run() {
        while (true) {
            try {
                buffer.pop().run();
            } catch (PosionPillException e) {
                break;
            } catch (Exception ignored) {}
        }
        try {
            workerCounter.decrement();
        } catch (InterruptedException ignored) {}
    }
}