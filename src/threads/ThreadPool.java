package threads;

import task.Buffer;

public class ThreadPool {
    private final Buffer buffer;
    private final int numThreads;
    
    private final WorkerCounter workerCounter;

    public ThreadPool(int numThreads, Buffer buffer, WorkerCounter workerCounter) {
        this.numThreads = numThreads;
        this.buffer = buffer;
        this.workerCounter = workerCounter;
    }

    public void startThreads(){
        for (int i = 0; i < numThreads; i++){
            Thread worker = new Worker(buffer, workerCounter);
            worker.start();
        }
    }

    public Buffer getBuffer() {
        return buffer;
    }

    public int getNumThreads() {
        return numThreads;
    }

    public WorkerCounter getWorkerCounter() {
        return workerCounter;
    }
}
